import {
  FormControl,
  FormHelperText,
  Grid,
  Input,
  InputLabel,
  Typography,
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import React, { useState } from "react";
import { SubmitButton } from "../elements";
import "./NewReservationComponent.css";
import AddBoxIcon from "@material-ui/icons/AddBox";
import { maxNumberOfGuests } from "../elements";

import { toast } from "react-toastify";
toast.configure({
  autoClose: 3000,
  draggable: false,
});

const moment = require("moment");

const validateEmail = (email) => {
  // eslint-disable-next-line no-useless-escape
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(String(email).toLowerCase());
};

const NewReservationComponent = (props) => {
  const [name, setName] = useState("");
  const [numberOfGuests, setNumberOfGuests] = useState("");
  const [email, setEmail] = useState("");
  const [datetime, setDateTime] = useState(
    moment(new Date()).format("YYYY-MM-DDTkk:mm")
  );
  const [errormessage, setErrorMessage] = useState("");
  const submitDisabled =
    !name ||
    !numberOfGuests ||
    !datetime ||
    numberOfGuests < 1 ||
    numberOfGuests > maxNumberOfGuests;

  const createReservation = () => {
    props.createReservation({ name, numberOfGuests, email, datetime });
    toast.info("New reservation recorded.");
    setName("");
    setEmail("");
    setDateTime(moment(new Date()).format("YYYY-MM-DDTkk:mm"));
    setNumberOfGuests("");
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setErrorMessage("");
    if (new Date(datetime).getTime() < new Date().getTime()) {
      setErrorMessage(
        "Selected date is in the past. Please select a valid date."
      );
    } else {
      if (!email) {
        createReservation();
      } else if (email && !validateEmail(email)) {
        setErrorMessage(
          "Your email address is invalid. Please enter a valid address."
        );
      } else {
        createReservation();
      }
    }
  };

  return (
    <div>
      <form
        noValidate
        autoComplete="off"
        className="formstyle"
        onSubmit={handleSubmit}
      >
        <AddBoxIcon className="addbox_icon"></AddBoxIcon>
        <Grid
          container
          direction="column"
          spacing={1}
          justify="center"
          alignItems="center"
        >
          <Grid item>
            <Typography variant="h4">Create a reservation:</Typography>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <InputLabel required className="inputLabel">
                Name
              </InputLabel>
              <Input
                label="Name"
                className="inputclass"
                onChange={(event) => setName(event.target.value)}
                value={name}
                required
              />
              <FormHelperText>We'll never share your name.</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <TextField
                className="inputclass"
                label="Date & time"
                value={datetime}
                type="datetime-local"
                InputLabelProps={{
                  shrink: true,
                }}
                defaultValue={moment(new Date()).format("YYYY-MM-DDTkk:mm")}
                onChange={(event) => setDateTime(event.target.value)}
                required
              />
            </FormControl>
            <FormHelperText>Format: yyyy-MM-ddThh:mm</FormHelperText>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <TextField
                className="inputclass"
                label="Number of guests"
                value={numberOfGuests}
                InputLabelProps={{
                  shrink: true,
                }}
                type="number"
                inputProps={{ min: "1", max: "40", step: "1" }}
                onChange={(event) => setNumberOfGuests(event.target.value)}
                required
              />
            </FormControl>
            <FormHelperText>Maximum {maxNumberOfGuests} people.</FormHelperText>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <InputLabel className="inputLabel">Email address</InputLabel>
              <Input
                className="inputclass"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
              <FormHelperText>Not required.</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item className="item_newreservation">
            <SubmitButton type="submit" disabled={submitDisabled} />
          </Grid>
          <Grid item className="errorhandling">
            <Typography color="error">{errormessage}</Typography>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default NewReservationComponent;
