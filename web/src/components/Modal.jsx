import {
  FormControl,
  FormHelperText,
  Grid,
  Input,
  Typography,
  TextField,
} from "@material-ui/core";
import React, { useState, useEffect, useRef } from "react";
import { SubmitButton } from "../elements";
import "./Modal.css";
import { maxNumberOfGuests } from "../elements";

import { toast } from "react-toastify";
toast.configure({
  autoClose: 3000,
  draggable: false,
});

const display = {
  display: "block",
};
const hide = {
  display: "none",
};

const validateEmail = (email) => {
  // eslint-disable-next-line no-useless-escape
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(String(email).toLowerCase());
};

const moment = require("moment");

const Modal = (props) => {
  const [name, setName] = useState(props.reservation.name);
  const [numberOfGuests, setNumberOfGuests] = useState(
    props.reservation.numberOfGuests
  );
  const [email, setEmail] = useState(props.reservation.email);
  const [datetime, setDateTime] = useState(props.reservation.datetime);
  const [errormessage, setErrorMessage] = useState("");

  const inputName = useRef(null);
  const inputEmail = useRef(null);
  const inputDatetime = useRef(null);
  const inputNumberOfGuests = useRef(null);

  const submitDisabled =
    !name ||
    !numberOfGuests ||
    !datetime ||
    numberOfGuests < 1 ||
    numberOfGuests > maxNumberOfGuests;

  const editReservation = () => {
    props.editReservation({
      name,
      numberOfGuests,
      email,
      id: props.reservation.id,
      datetime,
    });
    props.setModalToggle(false);
    toast.info(
      "The reservation (id: " + props.reservation.id + ")  has been updated."
    );
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setErrorMessage("");
    if (new Date(datetime).getTime() < new Date().getTime()) {
      setErrorMessage(
        "Selected date is in the past. Please select a valid date."
      );
    } else {
      if (!email) {
        editReservation();
      } else if (email && !validateEmail(email)) {
        setErrorMessage(
          "Your email address is invalid. Please enter a valid address."
        );
      } else {
        editReservation();
      }
    }
  };

  useEffect(() => {
    setName(props.reservation.name);
    setNumberOfGuests(props.reservation.numberOfGuests);
    setDateTime(props.reservation.datetime);
    setEmail(props.reservation.email);
  }, [props]);

  return (
    <div className="modal" style={props.toggle ? display : hide}>
      <form noValidate autoComplete="off" onSubmit={handleSubmit} style={{margin:"auto"}}>
        <Grid
          container
          direction="column"
          spacing={1}
          justify="center"
          alignItems="center"
        >
          <Grid item>
            <Typography variant="h4">Edit reservation:</Typography>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <Input
                ref={inputName}
                className="inputclass"
                onChange={(event) => setName(event.target.value)}
                value={name}
                required
              />
              <FormHelperText>We'll never share your name.</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <TextField
                ref={inputDatetime}
                className="inputclass"
                label="Date & time"
                type="datetime-local"
                defaultValue={moment(datetime).format("YYYY-MM-DDTkk:mm")}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(event) => setDateTime(event.target.value)}
                required
              />
              <FormHelperText>Format: mm/dd/yyyy, hh:mm AM|PM</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <TextField
                ref={inputNumberOfGuests}
                className="inputclass"
                label="Number of guests"
                InputLabelProps={{
                  shrink: true,
                }}
                type="number"
                value={numberOfGuests}
                inputProps={{ min: "1", max: { maxNumberOfGuests }, step: "1" }}
                onChange={(event) => setNumberOfGuests(event.target.value)}
                required
              />
            </FormControl>
            <FormHelperText>Maximum {maxNumberOfGuests} people.</FormHelperText>
          </Grid>
          <Grid item className="item_newreservation">
            <FormControl>
              <Input
                ref={inputEmail}
                className="inputclass"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
              <FormHelperText>Not required.</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item className="item_newreservation submit">
            <SubmitButton type="submit" disabled={submitDisabled} />
            <div
              className="cancelBtn"
              onClick={() => props.setModalToggle(false)}
            >
              Cancel
            </div>
          </Grid>
          <Grid item className="errorhandling">
            <Typography color="error">{errormessage}</Typography>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default Modal;
