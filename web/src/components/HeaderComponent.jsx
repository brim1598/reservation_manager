import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  header:{
    backgroundColor: "rgb(27,35,50)",
  },
  title: {
    flexGrow: 1,
  },
}));

const HeaderComponent = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.header}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            GRILLENIUM FALCON
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default HeaderComponent;
