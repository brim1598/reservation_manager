import React from "react";
import { FormHelperText, Grid, Input } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

const searchBarStyle = {
  marginLeft: "5px",
  marginBottom: "2%",
  display:"flex"
};

const searchIconStyle = {
  background: "linear-gradient(60deg, #ffa726, #fb8c00)",
  color: "white",
  fontSize: "60px",
  padding: "15px",
  marginRight:"15px",
  borderRadius: "10%",
  boxShadow:
    "0 12px 20px -10px rgba(255, 152, 0, 0.28), 0 4px 20px 0px rgba(0, 0, 0, 0.12), 0 7px 8px -5px rgba(255, 152, 0, 0.2)",
};

const SearchBarComponent = (props) => {
  const handleChange = (e) => {
    props.searchReservation(e.target.value);
  };

  return (
    <Grid container spacing={1}>
      <Grid item style={searchBarStyle}>
        <SearchIcon style={searchIconStyle}></SearchIcon>
        <div style={{ display: "inline-table" }}>
          <Input
            className="inputclass"
            placeholder = "Type here..."
            onChange={(e) => handleChange(e)}
          ></Input>
          <FormHelperText>Search reservation by name.</FormHelperText>
        </div>
      </Grid>
    </Grid>
  );
};

export default SearchBarComponent;
