import { confirmAlert } from "react-confirm-alert"; // Import
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import "./ConfirmAlert.css";

const confirmAlertFunc = (reservation, deleteReservation, popupFunc) => {
  confirmAlert({
    title: " Deleting Reservation",
    message: "Are you sure you want to delete this reservation?",
    buttons: [
      {
        label: "Confirm",
        onClick: () => {
          deleteReservation(reservation);
          popupFunc();
        },
        className: "alertConfirmBtn",
      },
      {
        label: "Cancel",
        className: "alertCancelBtn",
      },
    ],
  });
};

export default confirmAlertFunc;
