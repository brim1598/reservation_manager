import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import "./ReservationsComponent.css";
import EventSeatSharpIcon from "@material-ui/icons/EventSeatSharp";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Modal from "./Modal.jsx";
import confirmAlertFunc from "./ConfirmAlert.jsx";

import { toast } from "react-toastify";

toast.configure({
  autoClose: 3000,
  draggable: false,
});

const ReservationsComponent = (props) => {
  const { reservations, filteredReservations } = props;
  const [modalToggle, setModalToggle] = useState(false);
  const [selectedReservation, setSelectedReservation] = useState({});

  const onClickHandle = (type, id) => {
    const reservation = reservations.find((value) => value.id === id);
    if (type === "edit") {
      setModalToggle(true);
      setSelectedReservation(reservation);
    } else if (type === "delete") {
      const popupFunc = () => toast.info("The reservation has been deleted.");
      confirmAlertFunc(reservation, props.deleteReservation, popupFunc);
    }
  };

  return (
    <div>
      <Grid
        container
        direction="row"
        spacing={5}
        justify="center"
        alignItems="center"
        className="reservationContainer"
      >
        {filteredReservations.map((reservation, index) => (
          <Grid item className="reservation" key={index}>
            <EventSeatSharpIcon className="upper_left_icon"></EventSeatSharpIcon>
            <div className="res_header">
              <p>{reservation.name}</p>
            </div>
            <div className="res_content">
              <p>
                <span>Email:</span> {reservation.email ? reservation.email : ""}
              </p>
              <p>
                <span>Date:</span>{" "}
                {new Date(reservation.datetime).toLocaleString()}
              </p>
              <p>
                <span>Guests:</span> {reservation.numberOfGuests}
              </p>
            </div>
            <div className="res_footer">
              <EditIcon
                className="edit_icon"
                onClick={() => onClickHandle("edit", reservation.id)}
              ></EditIcon>
              <DeleteIcon
                className="delete_icon"
                onClick={() => onClickHandle("delete", reservation.id)}
              ></DeleteIcon>
            </div>
          </Grid>
        ))}
      </Grid>
      <Modal
        toggle={modalToggle}
        setModalToggle={setModalToggle}
        reservation={selectedReservation}
        editReservation={props.editReservation}
      ></Modal>
    </div>
  );
};

export default ReservationsComponent;
