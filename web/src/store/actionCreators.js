export const searchReservationType = "SEARCH_RESERVATIONS";

export function searchReservation(searchString) {
  return {
    type: searchReservationType,
    searchString,
  };
}

export const newReservationType = "NEW_RESERVATION";

export function newReservation(reservation) {
  return {
    type: newReservationType,
    reservation,
  };
}

export const editReservationType = "EDIT_RESERVATION";

export function editReservation(reservation) {
  return {
    type: editReservationType,
    reservation,
  };
}

export const deleteReservationType = "DELETE_RESERVATION";

export function deleteReservation(reservation) {
  return {
    type: deleteReservationType,
    reservation,
  };
}
