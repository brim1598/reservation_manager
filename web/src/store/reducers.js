import {
  newReservationType,
  searchReservationType,
  deleteReservationType,
  editReservationType,
} from "./actionCreators";

/*
  Reducers specify how the application's state changes in response to actions sent to the store. 
*/

const list = [
  {
    id: 1,
    name: "John Smith",
    numberOfGuests: 4,
    datetime: "2020-07-19T20:23:01.804Z",
    email: "john@gmail.com",
  },
  {
    id: 2,
    name: "Emily Parker",
    numberOfGuests: 2,
    datetime: "2020-07-20T15:43:54.776Z",
    email: "emily@gmail.com",
  },
  {
    id: 3,
    name: "Verona Blair",
    numberOfGuests: 5,
    datetime: "2020-07-20T14:53:38.634Z",
    email: "verona@gmail.com",
  },
  {
    id: 4,
    name: "Natalie Lee-Walsh",
    numberOfGuests: 3,
    datetime: "2020-07-25T14:39:34.527Z",
    email: "natalie@gmail.com",
  },
  {
    id: 5,
    name: "Tamzyn French",
    numberOfGuests: 4,
    datetime: "2020-07-21T12:39:34.527Z",
    email: "tamzyn@gmail.com",
  },
  {
    id: 6,
    name: "John Stone",
    numberOfGuests: 4,
    datetime: "2020-07-20T20:39:34.527Z",
    email: "stone@gmail.com",
  },
  {
    id: 7,
    name: "Salome Simoes",
    numberOfGuests: 3,
    datetime: "2020-07-21T17:43:54.776Z",
    email: "salome@gmail.com",
  },
];

const initialState = {
  list,
  searchString: "",
  filtered: list,
};

// https://www.samanthaming.com/tidbits/33-how-to-compare-2-objects/
// https://lodash.com/docs/#isEqual
const _ = require("lodash");

const removeItem = (items, payload) => {
  for (var i = 0; i < items.length; i++) {
    if (_.isEqual(items[i], payload)) {
      items.splice(i, 1);
    }
  }
  return items;
};

// https://stackoverflow.com/questions/4020796/finding-the-max-value-of-an-attribute-in-an-array-of-objects
const getMaxId = (list) => {
  // if exist a reservation without 'id' key
  const found = list.every((element) => element.id >= 0);
  if (!found) {
    console.error("Exist one or more reservations without 'id' key");
  }
  return Math.max.apply(
    Math,
    list.map((value) => {
      return value.id;
    })
  );
};

const addIdForReservation = (reservation, list) => {
  reservation["id"] = getMaxId(list) + 1;
  return reservation;
};

const updateReservations = (list, reservation) => {
  const newList = [...list].map((value) => {
    return reservation.id === value.id ? reservation : value;
  });
  return newList;
};

const searchReservationFunc = (list, string) => {
  const searchString = string.trim().toLowerCase();
  if (searchString.length > 0) {
    list = list.filter((item) => {
      return item.name.toLowerCase().match(searchString);
    });
  }
  return list;
};

const resetFilteredList = (state, newList) => {
  state.filtered = searchReservationFunc([...newList], state.searchString);
};

const sortFilteredListByNewestReservation = (state) => {
  state.filtered = [...state.filtered].sort((a, b) => {
    return b.id - a.id;
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case searchReservationType: {
      const newList = searchReservationFunc(
        [...state.list],
        action.searchString
      );
      state.searchString = action.searchString;
      return {
        ...state,
        filtered: newList,
      };
    }
    case newReservationType: {
      const newList = [...state.list];
      const reservation = addIdForReservation({ ...action.reservation }, [
        ...state.list,
      ]);
      newList.push(reservation);
      state.searchString = "";
      resetFilteredList(state, newList);
      sortFilteredListByNewestReservation(state);
      return {
        ...state,
        list: newList,
      };
    }
    case editReservationType: {
      const newList = updateReservations([...state.list], action.reservation);
      resetFilteredList(state, newList);
      sortFilteredListByNewestReservation(state);
      return {
        ...state,
        list: newList,
      };
    }
    case deleteReservationType: {
      const newList = removeItem([...state.list], action.reservation);
      resetFilteredList(state, newList);
      return {
        ...state,
        list: newList,
      };
    }
    default:
      return state;
  }
};

export default reducer;
