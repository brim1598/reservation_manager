import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { Provider } from "react-redux";
import {
  NewReservationContainer,
  ReservationsContainer,
  SearchBar,
} from "../containers";
import HeaderComponent from "../components/HeaderComponent";
import store from "../store/store";
import "./App.css";
import theme from "./theme";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <Provider store={store}>
      <MuiThemeProvider theme={theme}>
        <HeaderComponent></HeaderComponent>
        <Container maxWidth="lg">
          <div className="App">
            <Grid
              container
              spacing={2}
              style={{ marginTop: "0", overflowX: "auto" }}
            >
              <Grid item xs={12} className="app_grid">
                <NewReservationContainer />
              </Grid>
              <Grid item xs={12} className="app_grid searchbar">
                <SearchBar />
              </Grid>
              <Grid
                item
                xs={12}
                className="app_grid"
                style={{ overflow: "hidden" }}
              >
                <ReservationsContainer />
              </Grid>
            </Grid>
          </div>
        </Container>
      </MuiThemeProvider>
      <ToastContainer></ToastContainer>
    </Provider>
  );
}

export default App;
