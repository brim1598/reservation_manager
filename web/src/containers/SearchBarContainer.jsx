import { connect } from "react-redux";
import SearchBarComponent from "../components/SearchBarComponent";
import { searchReservation } from "../store/actionCreators";

// https://react-redux.js.org/using-react-redux/connect-mapdispatch
const mapDispatchToProps = (dispatch) => ({
  searchReservation: (searchString) => dispatch(searchReservation(searchString)),
});

// https://react-redux.js.org/api/connect
export default connect(undefined, mapDispatchToProps)(SearchBarComponent);
