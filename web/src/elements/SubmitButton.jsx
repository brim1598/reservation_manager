import { Button, createStyles, makeStyles } from "@material-ui/core";
import React from "react";

// Styling with material-ui hook API: https://material-ui.com/styles/basics/
const useStyles = makeStyles(() =>
  createStyles({
    button: {
      background: "rgb(94,175,81)",
      color: "white",
      boxShadow:
        "0 2px 2px 0 rgba(76, 175, 80, 0.14), 0 3px 1px -2px rgba(76, 175, 80, 0.2), 0 1px 5px 0 rgba(76, 175, 80, 0.12)",
    },
  })
);

const SubmitButton = (props) => {
  const classes = useStyles();
  return (
    <Button type="submit" disabled={props.disabled} className={classes.button}>
      Submit
    </Button>
  );
};

export default SubmitButton;
